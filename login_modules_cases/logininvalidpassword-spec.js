var frisby = require('frisby');

// Tester can change these parameters only
var user = "bademailaddress@gmail.com";
var pass = "InvalidPassword";

//get the current UTC time from this URL: (https://currentmillis.com/)
var now = new Date(); 
var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds())
var currentUTCTime = now_utc.getTime() - (now_utc.getTimezoneOffset() * 60000);


frisby.create('Authentication for InvalidPassword')
// user = "greataupairtest+serviceacct-at-greataupair.com@gmail.com";
//send the params to below url for user auth
   .post("http://dev-api.greataupairusa.com/index.cfm/session",
      { username: user,
        password: pass
      },
      { json: true,
      	headers: {Accept:'application/json',
                 'X-Application-Key':'7C6C819E-B474-497E-8F85-17C778DCCFC7',
                 'Content-Type':'application/json',
                 'x-locale':'en_US',
                 'x-microtime':currentUTCTime, //get the current UTC time
                 'auth-signature':'rTZwPhlq5hDrXPqbASR9+Li7UJ+XxxZLA7LVcAOxkuA=' //uses the old auth key first time
				 }
       }
    )

   //checked the expected result 401 first time if no error occured.
   .expectStatus(401)
    
    // get all the responses and find the new auth signature
   .after(function (err, res, body) {
	   	
   		//If we are not putting right time then message will print.
	   	console.log('Time UTC is not set proper:---',res.body.serverTime)

	   	// HMAC key stored into obj variable.
	   var obj = res.body.hmacHash;

	   // Again send the post call with new auth signature for successfull responses  	
   	   frisby.create('Post data requiring token authentication for InvalidPassword')
       .post("http://dev-api.greataupairusa.com/index.cfm/session",
            { username: user,
              password: pass
            },
            { json: true,
         	     headers: {Accept:'application/json',
                 'X-Application-Key':'7C6C819E-B474-497E-8F85-17C778DCCFC7',
                 'Content-Type':'application/json',
                 'x-locale':'en_US',
                 'x-microtime':currentUTCTime, //get the current UTC time
                 'auth-signature':obj // Added new auth-signature for new responses
			    }
            }
       )
       .expectStatus(200)

        //checked all the responses
        .after(function (err, res, body) {
		   	console.log(res.body)
		})

       .toss();
   })

.toss();